import React from "react";
import TitleBar from "../components/TitleBar/titleBar";
import List from "../components/List/list";
const MainPage = () => {
    return(
        <>
        <TitleBar />
        <List />
        </>
    )
};

export default MainPage;