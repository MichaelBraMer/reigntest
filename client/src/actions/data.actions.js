import axios from "axios";
import { api } from "../server.url.json";

export const RECIVE_DATA = "RECIVE_DATA";
export const SUCCESS_DATA = "SUCCESS_DATA";
export const DELETE_DATA = "DELETE_DATA";
export const ERROR_DATA = "ERROR_DATA";

const reciveData = () => {
    return {
        type: RECIVE_DATA
    };
};

const successData = (data) => {
    return {
        type: SUCCESS_DATA,
        data
    };
};

const deleteData = () => {
    return {
        type: DELETE_DATA
    }
}

const errorData = () => {
    return {
        type: ERROR_DATA
    }
}

export const getData = () => async(dispatch) => {
    try {
        dispatch(reciveData());
        const response = await axios.get(`${api}/data`);
        const { status } = response;
        if (status === 200) {
            const { data } = response;
            dispatch(successData(data));

        } else {
            dispatch(errorData());
        }
    } catch (error) {
        dispatch(errorData());
    }
};

export const eliminate = (id) => async(dispatch) => {
    try {
        dispatch(reciveData());
        const response = await axios.put(`${api}/eliminate/${id}`);
        const { status } = response;
        if (status === 200) {
            dispatch(deleteData());
        } else {
            dispatch(errorData());
        }
    } catch (error) {
        dispatch(errorData());
    }
}