import {
    ERROR_DATA,
    RECIVE_DATA,
    SUCCESS_DATA,
    DELETE_DATA
} from '../actions/data.actions'

export default (
    state = {
        data: [],
        reciveRequest: false,
        errorRequest: false,
        deleteRequest: false
    },
    action
) => {
    switch (action.type) {
        case RECIVE_DATA:
            return {
                ...state,
                reciveRequest: true
            };
        case SUCCESS_DATA:
            return {
                ...state,
                reciveRequest: false,
                deleteRequest: false,
                data: action.data
            }
        case DELETE_DATA:
            return {
                ...state,
                reciveRequest: false,
                deleteRequest: true
            }
        case ERROR_DATA:
            return {
                ...state,
                reciveRequest: false,
                errorRequest: true
            };
        default:
            return state;
    }
};