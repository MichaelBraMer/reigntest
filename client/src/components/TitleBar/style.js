import styled from "styled-components";
import { AppBar } from "@material-ui/core";

export const AppbarConfigure = styled(AppBar)`
  && {
    background-color: black;
    padding-bottom: 50px;
  }
`;

export const PrincipalTitle = styled.h1`
    padding-top: 49.5px;
    margin-left: 2rem;
    font-size: 62pt;
    font-family: Calibri;
    line-height: 0.5px;
    
`;

export const SecundaryTitle = styled.h3`
    margin-left: 2.5rem;
    font-size: 18pt;
    font-family: Calibri;
    line-height: 0.5px;
`;
