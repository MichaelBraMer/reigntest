import React from "react";
import { AppbarConfigure, PrincipalTitle, SecundaryTitle } from "./style";

const TitleBar = () => {
    return (
        <AppbarConfigure position="static">
            <PrincipalTitle>HN Feed</PrincipalTitle>
            <SecundaryTitle>{"We <3 hacker news!"}</SecundaryTitle>
        </AppbarConfigure>
    )
}

export default TitleBar;