import React, { useEffect, useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getData } from "../../actions/data.actions";
import Item from "../../components/Item/item";

const List = () => {
    const { data, deleteRequest } = useSelector(
        state => state.data
    );
    const dispatch = useDispatch();

    const callRequest = useCallback(async () => {
        dispatch(getData());
    }, [dispatch]);

    useEffect(() => {
        callRequest();
    }, [callRequest]);

    useEffect(() => {
        if (deleteRequest) {
            callRequest();
        }
    }, [deleteRequest, callRequest]);

    return (
        <>
            {
                data.map((data, index) => {
                    if (!data.deleted) {
                        return (<Item
                            key={index}
                            id={data.objectID}
                            title={data.title}
                            author={data.author}
                            url={data.url}
                            time={data.created_at}
                        />)
                    }
                    return "";
                })
            }
        </>
    )
}

export default List;