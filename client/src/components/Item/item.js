import React from "react";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { eliminate } from "../../actions/data.actions";
import { Divider, Tooltip } from "@material-ui/core";
import { Delete } from "@material-ui/icons";

import {
  Row,
  Author,
  Time,
  Title,
  TitleContent,
  DateContent,
  DeleteButton
} from "./style";

const Item = ({ title, time, author, id, url }) => {
  const dispatch = useDispatch();
  var clock = { hour:'2-digit', minute: '2-digit',hour12: true};
  var calendar= { day: 'numeric', month: 'short'}
  const today = new Date();
  const timeCurrent = new Date(time);
  var date;
  if (timeCurrent.getDate() === today.getDate()) {
    date = new Date(time).toLocaleTimeString('en-US', clock);
  } else if (timeCurrent.getDate() === today.getDate() - 1) {
    date = "Yesterday"
  } else {
    date = new Date(time).toLocaleDateString('en-US', calendar);
  }

  const handleClick = () => {
    dispatch(eliminate(id));
  };

  const goToUrl = () => {
    window.open(url);
  };

  return (
    <>
      <div style={{ paddingTop: "10px", paddingLeft: "52px", paddingRight: "24px" }}>
        <Row>
          <TitleContent onClick={goToUrl}> 
            <Title>{title} -</Title>
            <Author>{author} -</Author>
          </TitleContent>
          <DateContent onClick={goToUrl}>
            <Time>{date}</Time>
          </DateContent>
          <DeleteButton >
            <Tooltip title="Delete" onClick={handleClick}>
              <Delete />
            </Tooltip>
          </DeleteButton>
        </Row>
        <Divider />
      </div>
    </>
  );
};

Item.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired
};

export default Item;
