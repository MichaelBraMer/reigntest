import styled from "styled-components";

export const Row = styled.span`
  display: flex;
  background-color: #fff;
  border: 1px #ccc;
  color: white;
  cursor: pointer;
  &:hover {
    background-color: #fafafa;
    color: black;
  }
`;

export const Title = styled.h2`
  color: #333;
  font-size: 13pt
`;

export const Time = styled.h2`
  color: #333;
  font-size: 13pt
`;

export const Author = styled.h2`
  color: #999;
  font-size: 13pt
`;

export const TitleContent = styled.span`
  display: flex;
  flex: 3;
  justify-content: flex-start;
  cursor: pointer
`;

export const DateContent = styled.span`
  display: flex;
  flex: 3;
  justify-content: center
`;

export const DeleteButton = styled.span`
  display: flex;
  font-size: 25pt;
  align-items: center;
  cursor: pointer
`;