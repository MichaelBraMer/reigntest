import React from 'react';
import { Provider } from 'react-redux';
import configureStore from './store/store';
import MainPage from './pages/MainPage'
const store = configureStore();

const App = () => {
    return (
        <Provider store={store}>
            <MainPage />
        </Provider>
    );
};

export default App;