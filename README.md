# REIGN TEST
This project was created by Michael Bravo Mery

## Technologies

- Frontend - Client: React + Material UI
- Backend - Server: Node js

## Install dependencies

### Server
To install the dependencies:

- `$ cd .\server\ `
- `$ npm install`

### Client
To install the dependencies:
- `$ cd .\client\ `
- `$ npm install`


## Run Server & Client

### Server
To start the server:

- `$ npm run dev `

### Client
To start the client:

- `$ npm start `

## Test & Coverage

To run only test:

- `$ npm run test `

To run test and coverage:

- `$ npm run coverage`
## Docker

### Server

To run docker in server:

- `$ npm run docker-container`

### Client

To run docker in client:

- `$ npm run docker-container`