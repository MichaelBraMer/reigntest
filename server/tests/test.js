import request from "supertest";
import { expect } from "chai";
import server from "../src/server";
import { deleteSimulator } from "../src/helpers/deleteSimulator"
describe("GET DATA", () => {
    it("Recive a list whit all data of database", (done) => {
        request(server)
            .get("/api/data")
            .expect("Content-Type", /json/)
            .expect(200, done);
    });
});

describe("SET DELETE FROM DATA", () => {

    it("Recive a json whit a success message", (done) => {

        const { status, msg } = deleteSimulator("24609116")
        expect(status).to.equal(200)
        expect(msg).to.equal("El dato fue eliminado con exito");
        done();
    })

    it("Recive a json whit a error message", (done) => {
        request(server)
            .put("/api/eliminate/1")
            .end((error, response) => {
                expect("Content-Type", /json/)
                expect(response.statusCode).to.equal(404)
                expect(response.body.msg).to.equal("El dato no existe")
                error ? done(error) : done()
            })
    })
})

describe("ROUTE NOT FOUND", () => {
    it("Recive a json whit a not found message", (done) => {
        request(server)
            .get("/api/notfound")
            .end((error, response) => {
                expect("Content-Type", /json/);
                expect(response.statusCode).to.equal(404);
                expect(response.body.message).to.equal("La ruta de la solicitud HTTP no es reconocida por el servidor.");
                error ? done(error) : done();
            })
    })
})