import mongoose, { connections } from "mongoose";

const DB_NAME= process.env.DB_NAME || 'testReign';
const DB_HOST= process.env.DB_HOST || 'localhost';

const dbUri = `mongodb://${DB_HOST}/${DB_NAME}`;

mongoose.connect(dbUri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const connection = mongoose.connection;

connection.once("open", () => {
    console.log("Database is connected")
})