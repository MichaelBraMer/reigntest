import axios from "axios";
import dataModel from "../models/data.model";

export const loadData = async() => {
    const eliminateds = await dataModel.find({ deleted: true });

    await dataModel.remove({});

    const { status, data } = await axios.get("https://hn.algolia.com/api/v1/search_by_date?query=nodejs");

    if (status === 200) {
        const { hits } = data;
        if (hits.length > 0) {
            hits.forEach(async(hit) => {
                const {
                    created_at,
                    title,
                    url,
                    author,
                    story_title,
                    story_url,
                    objectID,
                } = hit;

                if (title || story_title) {
                    const eliminated = eliminateds.find(
                        (eliminated) => eliminated.hit_id === objectID
                    );

                    var isDeleted = (eliminated) ? true : false;

                    const hit = new dataModel({
                        title: title || story_title,
                        url: url || story_url,
                        objectID,
                        author,
                        created_at,
                        deleted: isDeleted
                    });
                    await hit.save();
                }
            });
        }
    }
}