export const deleteSimulator = (id) =>{
    
    const data = [{
        _id:"5f7138600d0ba833a4c9191c",
        title:"Hal Finney’s Proposal for Optimizing Bitcoin to Be Enabled in Bitcoin Core",
        url: "https://www.btctimes.com/news/hal-finneys-proposal-for-optimizing-bitcoin-to-be-enabled-in-bitcoin-core",
        objectID:"24609116",
        author:"vmception",
        created_at:"2020-09-27T18:45:22.000Z",
        deleted:false,
        __v:0
    }];

    var curr = data.find((data)=> data.objectID=== id);

    if(!curr){
        return {
            status: 404,
            msg: "El dato no existe"
        }
    }else{
        curr.deleted = true;
        return{
            status: 200,
            msg: "El dato fue eliminado con exito"
        }
    }

}