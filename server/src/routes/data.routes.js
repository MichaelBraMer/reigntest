import { Router } from "express";
import dataFunctions from "../controllers/data.controller";

const router = Router();

router.get('/data', dataFunctions.getData);

router.put('/eliminate/:id', dataFunctions.delete);

router.all("*", (req, res) => {
    res.status(404).json({
        message: "La ruta de la solicitud HTTP no es reconocida por el servidor.",
    });
});

export default router;