import { Schema, model } from "mongoose";

const dataSchema = new Schema({
    title: String,
    url: String,
    objectID: String,
    author: String,
    created_at: String,
    deleted: Boolean

})

export default model("data", dataSchema);