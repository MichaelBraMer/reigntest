import express from "express";
import cors from "cors";
import morgan from "morgan";
import dataRoutes from "./routes/data.routes";
import cron from 'node-cron';
import "./helpers/connectionDB";
import { loadData } from "./helpers/loadData";

const app = express();

app.use(cors());

app.use(morgan("dev"));

app.set("port", process.env.PORT || 4000);

cron.schedule('* * */1 * *', () => {
    loadData();
});

app.use("/api", dataRoutes);

app.use("/", (req, res) => {
    res.send("Api desde express");
})


app.listen(app.get("port"), () => {
    console.log(`El servidor se esta ejecutando en el puerto ${app.get("port")}`);
})

export default app;