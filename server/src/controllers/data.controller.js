import dataModel from "../models/data.model";

const dataFunctions = {};

dataFunctions.getData = async(req, res) => {
    try {
        const data = await dataModel.find().sort({ created_at: -1 });
        return res.status(200).json(data);
    } catch (error) {
        return res.status(500).json({
            msg: 'Error al obtener los datos',
            error
        })
    }
}

dataFunctions.delete = async(req, res) => {
    try {
        const { id } = req.params;

        const hit = await dataModel.findOne({ objectID: id });

        if (!hit) {
            return res.status(404).json({
                msg: "El dato no existe"
            });
        }

        await dataModel.update({
            objectID: id
        }, {
            $set: {
                deleted: true
            }
        })

        await res.status(200).json({
            msg: "Dato eliminado con exito"
        })

    } catch (error) {
        return res.status(500).json({
            msg: "Error al eliminar dato",
            error
        })
    }
}
export default dataFunctions;